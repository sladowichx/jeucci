import { Injectable } from '@angular/core';
import { Question } from '../Models/Question';
import { Reponse } from '../Models/Reponse';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  questions:any
  datas:any = [
    {
      question:"Quelle est la capitale de la France ?",
      reponse1: new Reponse(1,"Dublin"),
      reponse2: new Reponse(2,"Paris"),
      reponse3: new Reponse(3,"Madrid"),
      reponse4: new Reponse(4,"Lisbonne"),
      bonne: 2
    },
    {
      question:"Combien de doigts a une main ?",
      reponse1: new Reponse(1,"10"),
      reponse2: new Reponse(2,"40"),
      reponse3: new Reponse(3,"5"),
      reponse4: new Reponse(4,"Pi"),
      bonne: 3
    },
    {
      question:"Combien y-a-il de film Star Wars ?",
      reponse1: new Reponse(1,"12"),
      reponse2: new Reponse(2,"9"),
      reponse3: new Reponse(3,"6"),
      reponse4: new Reponse(4,"132"),
      bonne: 1
    },
    {
      question:"En quelle année est sortie la PS4 ?",
      reponse1: new Reponse(1,"2019"),
      reponse2: new Reponse(2,"2012"),
      reponse3: new Reponse(3,"2016"),
      reponse4: new Reponse(4,"2013"),
      bonne: 4
    },
    {
      question:"Comment s'appelle le héro dans le jeu Assassin's creed ?",
      reponse1: new Reponse(1,"Altaïr"),
      reponse2: new Reponse(2,"Krayt"),
      reponse3: new Reponse(3,"Ezio"),
      reponse4: new Reponse(4,"Arkos"),
      bonne: 1
    },
    {
      question:"Quel âge à la reine Elizabeth ?",
      reponse1: new Reponse(1,"120"),
      reponse2: new Reponse(2,"200"),
      reponse3: new Reponse(3,"94"),
      reponse4: new Reponse(4,"89"),
      bonne: 3
    },
    {
      question:"Pain au chocolat ou Chocolatine ? ",
      reponse1: new Reponse(1,"Pain au chocolat"),
      reponse2: new Reponse(2,"Pain en forme de lune"),
      reponse3: new Reponse(3,"Chocolatine"),
      reponse4: new Reponse(4,"..."),
      bonne: 3
    },
    {
      question:"Depuis quand sommes nous confinés ?",
      reponse1: new Reponse(1,"mardi 17 mars"),
      reponse2: new Reponse(2,"Bien trop longtemps"),
      reponse3: new Reponse(3,"ça doit faire 2 jours"),
      reponse4: new Reponse(4,"lundi 11 mai"),
      bonne: 1
    },
    {
      question:"Combien font 258 * 3621 ?",
      reponse1: new Reponse(1,"Oula environ 10"),
      reponse2: new Reponse(2,"934218"),
      reponse3: new Reponse(3,"9342118"),
      reponse4: new Reponse(4,"218934"),
      bonne: 2
    },
    {
      question:"Quand serons nous déconfinés (normalement) ?",
      reponse1: new Reponse(1,"lundi 11 mai"),
      reponse2: new Reponse(2,"Je n'y crois plus"),
      reponse3: new Reponse(3,"Je n'ai jamais été confiné"),
      reponse4: new Reponse(4,"Mardi"),
      bonne: 1
    }
  ]

  constructor() { }

  LoadQuestions(){
    this.questions = new Array()
    this.datas.forEach(data => {
      var question = new Question(data.question,data.reponse1,data.reponse2,data.reponse3,data.reponse4,data.bonne);
      this.questions.push(question);
    });

    return this.melangeArray(this.questions);
  }

  // Mélange les réponses avant des les afficher
  melangeArray(array) {
    var currentIndex = array.length
    var temporaryValue: any
    var randomIndex: number;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Determine un index au hasard
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // Echange de place deux réponses dans le tableau
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }
}
