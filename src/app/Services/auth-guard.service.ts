import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router) { }

  /**
   * Contrôle qu'il y a bien un utilisateur en cours de partout pour accéder au jeu
   */
  canActivate() {
      const currentUser = localStorage.getItem("name_joueur") ;
      console.log(currentUser);
      
      if (currentUser != null && currentUser != "" && typeof currentUser != "undefined") {
          return true;
      }
      this.router.navigate(['/accueil'])
      return false;
  }

  joueurExistant(name:string){
    var listeJoueur = JSON.parse(localStorage.getItem("tab_scores"))
    if (listeJoueur != null) {
      var joueurExistant = listeJoueur.find(joueur => joueur.joueur == name.toLowerCase());
      if(typeof joueurExistant == "undefined"){
        return false;
      } else{
        return true;
      }   
    }         
  }
}
