import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {
  private tabScore:any

  constructor() { }

  getScore():string {
    return localStorage.getItem("tab_scores")
  }

  setScore(joueur:string,score:string){   
    var listeScore = localStorage.getItem("tab_scores");    
    var date = Date.now();
    if (listeScore != null) {
      this.tabScore = JSON.parse(listeScore);
      this.tabScore.push({joueur:joueur.toLowerCase(), score:score,date:date})
    } else {
      this.tabScore = new Array({joueur:joueur.toLowerCase(), score:score,date:date})
    }
        
    localStorage.setItem("tab_scores", JSON.stringify(this.tabScore));
  }
}
