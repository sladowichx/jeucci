import { Component, OnInit } from '@angular/core';
import { ScoreService } from '../Services/score.service';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css']
})
export class ScoreComponent implements OnInit {
  listeJoueurs:any
  constructor(private scoreService:ScoreService) { }

  ngOnInit(): void {
    this.listeJoueurs = JSON.parse(this.scoreService.getScore())  
    // Trie descendant par score
    if (this.listeJoueurs != null) {
      this.listeJoueurs.sort((a,b) => b.score - a.score)
    }    
  }

  // Efface tout le tableau des scores
  clearScore(){
    localStorage.clear();
    this.listeJoueurs = null;
  }

  // Supprime le joueur du tableau des scores
  delete(name:string){   
    var joueur = this.listeJoueurs.find(joueur => joueur.joueur == name)
    var index = this.listeJoueurs.indexOf(joueur)    
    if (index !== -1) {
      this.listeJoueurs.splice(index, 1);
      localStorage.setItem("tab_scores", JSON.stringify(this.listeJoueurs));
    }    
  }

}
