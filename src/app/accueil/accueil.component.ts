import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuestionService } from '../Services/question.service';
import { AuthGuardService } from '../services/auth-guard.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  nameJoueur:string;

  constructor(private router:Router, private authGuardService:AuthGuardService) { }

  ngOnInit(): void {
    localStorage.removeItem("name_joueur");  
  }

  Launch(name:string){
    if (typeof name != "undefined" || name != null) {
      if (name.length <= 20) {
        // Persistance du nom du joueur
        var joueurExistant = this.authGuardService.joueurExistant(name);
        if(!joueurExistant){
          localStorage.setItem('name_joueur',name)
          this.router.navigate(['jeu'])
        } else{
          alert("Le nom '" + name + "' existe déjà dans le tableau des scores!!!")
        }     
      }      
    }  
  }
}
