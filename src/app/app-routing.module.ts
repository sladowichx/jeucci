import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { JeuComponent } from './jeu/jeu.component';
import { ScoreComponent } from './score/score.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service'


const routes: Routes = [
  { path: 'accueil', component: AccueilComponent},
  { path: '', redirectTo:'accueil', pathMatch:'full'},
  { path: 'jeu', component: JeuComponent,canActivate: [AuthGuard]},
  { path: 'score', component: ScoreComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
