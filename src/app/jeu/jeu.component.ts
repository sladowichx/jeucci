import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../Services/question.service';
import { Router } from '@angular/router';
import { ScoreService } from '../Services/score.service';

@Component({
  selector: 'app-jeu',
  templateUrl: './jeu.component.html',
  styleUrls: ['./jeu.component.css']
})
export class JeuComponent implements OnInit {
  nameJoueur:string
  score:number;
  next:boolean;
  afficherScore:boolean;
  listeQuestions:any;
  nextQuestion:any;  
  nbQuestion:number;

  constructor(private scoreService:ScoreService,private questionService:QuestionService,private router:Router) { }

  ngOnInit(): void {
    this.next = false;
    this.afficherScore = false;
    this.score = 0;
    this.nbQuestion = 0;
    this.nameJoueur = localStorage.getItem("name_joueur")    
    this.listeQuestions = this.questionService.LoadQuestions();    
    this.NextQuestion();
  }

  NextQuestion(){
    if(this.nbQuestion < this.listeQuestions.length){
      this.nextQuestion = JSON.stringify(this.listeQuestions[this.nbQuestion]);    
      this.next = true;
      this.nbQuestion += 1;
    } else {
      this.next = false;
      this.scoreService.setScore(this.nameJoueur,this.score.toString())
      localStorage.removeItem("name_joueur"); 
      this.afficherScore = true;
    }   
  }

  CheckReponse(correct:boolean){
    if(correct){
      this.score += 1;
      console.log("Bravo!!!"); 
    } else {
      console.log("Réponse Fausse!!!"); 
    }
    console.log("Votre Score est de : " + this.score + " points!!");
    this.NextQuestion();    
  }
}
