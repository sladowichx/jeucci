import { Reponse } from './Reponse';

export class Question {
    private question:string;
    private reponse1:Reponse;
    private reponse2:Reponse;
    private reponse3:Reponse;
    private reponse4:Reponse;
    private bonneReponse:number;
    
    constructor(
        question:string, 
        reponse1:Reponse,
        reponse2:Reponse,
        reponse3:Reponse,
        reponse4:Reponse,
        bonneReponse:number){
            this.question = question;
            this.reponse1 = reponse1;
            this.reponse2 = reponse2;
            this.reponse3 = reponse3;
            this.reponse4 = reponse4;
            this.bonneReponse = bonneReponse;
    }

    getQuestion():string {
        return this.question;
    }

    getReponse1():Reponse{
        return this.reponse1;
    } 

    getReponse2():Reponse{
        return this.reponse2;
    }      

    getReponse3():Reponse{
        return this.reponse3;
    }

    getReponse4():Reponse{
        return this.reponse4;
    }
    
    getBonneReponse():number {
        return this.bonneReponse;
    }    
}