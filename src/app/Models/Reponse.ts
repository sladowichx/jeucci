export class Reponse{
    private id:number;
    private reponse:string

    constructor(id:number, reponse:string){
        this.id = id;
        this.reponse = reponse;
    }

    getId(){
        return this.id;
    }

    setId(id:number){
        this.id = id;
    }

    getReponse(){
        return this.reponse;
    }

    setReponse(reponse:string){
        this.reponse = reponse;
    }
}