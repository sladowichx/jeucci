import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { QuestionService } from '../Services/question.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  @Output() sendRequestResponse = new EventEmitter();
  @Input() question:string;
  @Input() nbQuest:number;
  nextQuestion:any
  quest:string;
  reponses:any;
  listeReponses:any;
  bonne:number;
  correct:boolean;

  constructor(private questionService:QuestionService) { }

  ngOnInit(): void {      
    
  }

  ngOnChanges():void{
    this.nextQuestion = JSON.parse(this.question)
    this.correct = false;
    this.quest = this.nextQuestion.question;

    this.listeReponses = new Array(
      this.nextQuestion.reponse1,
      this.nextQuestion.reponse2,
      this.nextQuestion.reponse3,
      this.nextQuestion.reponse4
    );

    this.reponses = this.questionService.melangeArray(this.listeReponses)    

    this.bonne = this.nextQuestion.bonneReponse;
  }  

  Repondre(idRreponse:any):void{ 
    if(idRreponse == this.bonne){
      this.correct = true;           
    }    

    this.sendRequestResponse.emit(this.correct);
  }
}
